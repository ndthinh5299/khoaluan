/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kltn.app.web.rest.vm;
